#! /bin/sh
### BEGIN INIT INFO
# Provides:          gmetad
# Required-Start:    $network $named $remote_fs $syslog
# Required-Stop:     $network $named $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Ganglia Meta Daemon
### END INIT INFO
. /lib/lsb/init-functions

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/usr/sbin/gmetad
NAME=gmetad
CONFFILE=/etc/ganglia/gmetad.conf
LOCKFILE=/var/lock/subsys/gmetad
DESC="Ganglia Meta Daemon"

test -x $DAEMON || { echo "$DAEMON not installed";
	if [ "$1" = "stop" ]; then exit 0;
	else exit 5; fi; }

case "$1" in

start)
    [ -f $CONFFILE ] || exit 6
    echo -n "Starting gmetad: "
    $DAEMON -c $CONFFILE
    RETVAL=$?
    echo
    [ $RETVAL -eq 0 ] && touch $LOCKFILE
    return $RETVAL
    ;;

stop)
    echo -n "Shutting down gmetad: "
    killproc $DAEMON
    RETVAL=$?
    echo
    [ $RETVAL -eq 0 ] && rm -f $LOCKFILE
    return $RETVAL
    ;;

status)
    if [ -f $LOCKFILE ]
      then
          echo "gmetad is running."
    else
          echo "gmetad is not running."
    fi
    ;;

restart|force-reload)
	$0 stop
	sleep 1
	$0 start
	;;

*)
    N=/etc/init.d/$NAME
	echo "Usage: $N {start|stop|status|restart|force-reload}" >&2
	exit 1
	;;
esac

exit 0
