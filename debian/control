Source: ganglia
Section: net
Priority: optional
Maintainer: Marcos Fouces <marcos@debian.org>
Homepage: http://ganglia.info/
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (=13), librrd-dev, libapr1-dev, libexpat1-dev, libconfuse-dev, po-debconf, libxml2-dev, libdbi0-dev, libpcre2-dev, gperf, rsync, libkvm-dev [kfreebsd-any], pkgconf, libz-dev, libtirpc-dev
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/debian/ganglia.git
Vcs-Browser: https://salsa.debian.org/debian/ganglia
Rules-Requires-Root: no

Package: ganglia-monitor
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, adduser, libganglia1t64 (=${binary:Version})
Pre-Depends: ${misc:Pre-Depends}
Description: cluster monitoring toolkit - node daemon
 Ganglia is a scalable, real-time cluster monitoring environment
 that collects cluster statistics in an open and well-defined XML format.
 .
 This package contains the monitor core program.

Package: gmetad
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, adduser, libganglia1t64 (=${binary:Version})
Pre-Depends: ${misc:Pre-Depends}
Suggests: ganglia-monitor, ganglia-webfrontend
Description: cluster monitoring toolkit - Ganglia Meta-Daemon
 Ganglia is a scalable, real-time cluster monitoring environment
 that collects cluster statistics in an open and well-defined XML format.
 .
 This package contains the 'gmetad' daemon, which collects information from
 ganglia monitor clients and writes it to RRD databases.

Package: libganglia1t64
Provides: ${t64:Provides}
Replaces: libganglia1
Breaks: libganglia1 (<< ${source:Version})
Architecture: any
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: cluster monitoring toolkit - shared libraries
 Ganglia is a scalable, real-time cluster monitoring environment
 that collects cluster statistics in an open and well-defined XML format.
 .
 This package contains shared libraries.

Package: libganglia1-dev
Architecture: any
Section: libdevel
Depends: ${misc:Depends}, libganglia1t64 (=${binary:Version}), ${shlibs:Depends}
Description: cluster monitoring toolkit - development libraries
 Ganglia is a scalable, real-time cluster monitoring environment
 that collects cluster statistics in an open and well-defined XML format.
 .
 This package contains development libraries.
